﻿using API.BusinessLogic;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class DirectoryController : ApiControllerBase
{
    private readonly IDirectoryStructureService directoryStructureService;

    public DirectoryController(IDirectoryStructureService directoryStructureService)
    {
        this.directoryStructureService = directoryStructureService;
    }

    [HttpGet("directory")]
    public async Task<IActionResult> GetDirectoryContents([FromQuery] string root, [FromQuery] string? directory = "")
    {
        try
        {
            return Ok(await directoryStructureService.GetFolderStructureForRootAndDirectoryAsync(root, directory));
        }
        catch (RootForbiddenException e)
        {
            return StatusCode(403, new
            {
                e.Message
            });
        }
    }

    [HttpGet("directories")]
    public async Task<IActionResult> GetFolderTree([FromQuery] string? path = "")
    {
        return Ok(await directoryStructureService.GetFolderStructureAsync(path));
    }

    [HttpGet("files")]
    public async Task<IActionResult> GetFiles([FromQuery] string path)
    {
        return Ok(await directoryStructureService.GetFilesAsync(path));
    }

    [HttpGet("roots")]
    public IActionResult GetPermittedRoots()
    {
        return Ok(new
        {
            Data = directoryStructureService.GetPermittedRoots()
        });
    }
}