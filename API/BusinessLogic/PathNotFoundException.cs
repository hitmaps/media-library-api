﻿namespace API.BusinessLogic;

public class PathNotFoundException : Exception
{
    public PathNotFoundException(string path) : base($"{path} does not exist on the filesystem")
    {
    }
}