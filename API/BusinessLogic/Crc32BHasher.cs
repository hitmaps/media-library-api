using System.Text;
using Force.Crc32;

namespace API.BusinessLogic;

public static class Crc32BHasher
{
    public static uint Crc32(string input)
    {
        return Crc32Algorithm.Compute(Encoding.ASCII.GetBytes(input));
    }
}