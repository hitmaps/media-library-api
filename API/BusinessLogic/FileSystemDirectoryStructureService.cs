﻿using API.BusinessLogic.IoiApi;

namespace API.BusinessLogic;

public class FileSystemDirectoryStructureService : IDirectoryStructureService
{
    private readonly string mediaLibraryDirectory;
    private readonly List<string> approvedRoots;
    private readonly IHitmapsIoiApiService hitmapsIoiApiService;

    private Dictionary<string, string> repoCache = new();
    private Dictionary<uint, string> locrCache = new();

    public FileSystemDirectoryStructureService(IConfiguration configuration,
                                     IHitmapsIoiApiService hitmapsIoiApiService)
    {
        mediaLibraryDirectory = configuration.GetValue<string>("MediaLibraryDirectory");
        approvedRoots = configuration.GetValue<string>("ApprovedMediaLibraryRoots")
            .Split(';')
            .Select(x => x.ToLowerInvariant())
            .ToList();
        this.hitmapsIoiApiService = hitmapsIoiApiService;
    }

    public async Task<Directory> GetFolderStructureAsync(string? path = "")
    {
        var currentDirectory = mediaLibraryDirectory;
        var directoryParts = path.Split("/");
        var rootsOnly = false;
        if (string.IsNullOrWhiteSpace(path))
        {
            directoryParts = Array.Empty<string>();
            rootsOnly = true;
        }
        foreach (var directoryPart in directoryParts)
        {
            var folderDoesNotExist = System.IO.Directory.GetFileSystemEntries(currentDirectory).All(x =>
                x.Replace(Path.Join(currentDirectory, Path.DirectorySeparatorChar.ToString()), string.Empty) !=
                directoryPart);
            var forbiddenRoot = directoryPart == directoryParts[0] && !approvedRoots.Contains(directoryPart);
            if (folderDoesNotExist || forbiddenRoot)
            {
                throw new PathNotFoundException(Path.Join(currentDirectory, directoryPart));
            }
            
            currentDirectory = Path.Join(currentDirectory, directoryPart);
        }
        
        var fileSystemEntries = System.IO.Directory.GetFileSystemEntries(currentDirectory);
        var structure = new Directory(string.Empty);
        foreach (var entry in fileSystemEntries)
        {
            var fileAttributes = System.IO.File.GetAttributes(entry);
            if (fileAttributes.HasFlag(FileAttributes.Directory))
            {
                var directoryName = new DirectoryInfo(entry).Name;
                if (!rootsOnly || approvedRoots.Contains(directoryName))
                {
                    var recursivePath = path == string.Empty ? directoryName : $"{path}/{directoryName}";
                    structure.Subdirectories.Add(new Directory(entry)
                    {
                        Subdirectories = (await GetFolderStructureAsync(recursivePath)).Subdirectories
                    });
                }
            }/*
            else
            {
                var file = new File(entry)
                {
                    FullPath = entry.Replace($"{mediaLibraryDirectory}{Path.DirectorySeparatorChar}", "img/")
                        .Replace(Path.DirectorySeparatorChar, '/')
                };
                
                var rootToRemove = directoryParts.Any() ? 
                    $"{Path.Join(mediaLibraryDirectory, directoryParts[0])}{Path.DirectorySeparatorChar}" : 
                    mediaLibraryDirectory; 
                var key = entry.Replace(rootToRemove, "images/")
                    .Replace(Path.DirectorySeparatorChar, '/');
                if (repoCache.TryGetValue(key, out var repoValue))
                {
                    var hashedLocrKey = Crc32BHasher.Crc32(repoValue.ToUpperInvariant());
                    var locrValue = locrCache.TryGetValue(hashedLocrKey, out var value) ? value : repoValue;
                    file.LocalizedTextName = locrValue;
                }

                structure.Files.Add(file);
            }*/
        }

        return structure;
    }

    public async Task<List<File>> GetFilesAsync(string path)
    {
        var currentDirectory = mediaLibraryDirectory;
        var directoryParts = path.Split("/");
        
        // Verify path
        foreach (var directoryPart in directoryParts)
        {
            var folderDoesNotExist = System.IO.Directory.GetFileSystemEntries(currentDirectory).All(x =>
                x.Replace(Path.Join(currentDirectory, Path.DirectorySeparatorChar.ToString()), string.Empty) !=
                directoryPart);
            var forbiddenRoot = directoryPart == directoryParts[0] && !approvedRoots.Contains(directoryPart);
            if (folderDoesNotExist || forbiddenRoot)
            {
                throw new PathNotFoundException(Path.Join(currentDirectory, directoryPart));
            }
            
            currentDirectory = Path.Join(currentDirectory, directoryPart);
        }
        
        // We reached here, so the path is valid. Return all directories / items in this path.
        if (!repoCache.Any())
        {
            repoCache = await hitmapsIoiApiService.GetRepoEntriesAsync();
        }

        if (!locrCache.Any())
        {
            locrCache = await hitmapsIoiApiService.GetLocalizedTextAsync();
        }
        
        var fileSystemEntries = System.IO.Directory.GetFileSystemEntries(currentDirectory);
        var files = new List<File>();
        foreach (var entry in fileSystemEntries)
        {
            var fileAttributes = System.IO.File.GetAttributes(entry);
            if (!fileAttributes.HasFlag(FileAttributes.Directory))
            {
                var file = new File(entry)
                {
                    FullPath = entry.Replace($"{mediaLibraryDirectory}{Path.DirectorySeparatorChar}", "img/")
                        .Replace(Path.DirectorySeparatorChar, '/')
                };

                var rootToRemove = directoryParts.Any() ?
                    $"{Path.Join(mediaLibraryDirectory, directoryParts[0])}{Path.DirectorySeparatorChar}" :
                    mediaLibraryDirectory;
                var key = entry.Replace(rootToRemove, "images/")
                    .Replace(Path.DirectorySeparatorChar, '/');
                if (repoCache.TryGetValue(key, out var repoValue))
                {
                    var hashedLocrKey = Crc32BHasher.Crc32(repoValue.ToUpperInvariant());
                    var locrValue = locrCache.TryGetValue(hashedLocrKey, out var value) ? value : repoValue;
                    file.LocalizedTextName = locrValue;
                }

                files.Add(file);
            }
        }

        return files;
    }

    public async Task<DirectoryWithFiles> GetFolderStructureForRootAndDirectoryAsync(string root, string? directory = "", bool includeFiles = true)
    {
        if (!approvedRoots.Contains(root.ToLowerInvariant()))
        {
            throw new RootForbiddenException(root);
        }

        if (!repoCache.Any())
        {
            repoCache = await hitmapsIoiApiService.GetRepoEntriesAsync();
        }

        if (!locrCache.Any())
        {
            locrCache = await hitmapsIoiApiService.GetLocalizedTextAsync();
        }

        directory ??= string.Empty;
        directory = directory.Replace('/', Path.DirectorySeparatorChar);
        var fullRootPath = $"{mediaLibraryDirectory}{Path.DirectorySeparatorChar}{root}";
        var filesystemEntries =
            System.IO.Directory.GetFileSystemEntries($"{fullRootPath}{Path.DirectorySeparatorChar}{directory}", 
                "*", 
                SearchOption.TopDirectoryOnly);

        var structure = new DirectoryWithFiles(directory);
        foreach (var entry in filesystemEntries)
        {
            var fileAttributes = System.IO.File.GetAttributes(entry);
            if (fileAttributes.HasFlag(FileAttributes.Directory))
            {
                structure.Directories.Add(await GetFolderStructureForRootAndDirectoryAsync(root, entry.Replace($"{fullRootPath}{Path.DirectorySeparatorChar}", string.Empty), false));
            }
            else if (includeFiles)
            {
                var file = new File(entry)
                {
                    FullPath = entry.Replace($"{fullRootPath}{Path.DirectorySeparatorChar}", $"img/{root}/")
                        .Replace(Path.DirectorySeparatorChar, '/')
                };
                var key = entry.Replace($"{fullRootPath}{Path.DirectorySeparatorChar}", "images/")
                    .Replace(Path.DirectorySeparatorChar, '/');
                if (repoCache.TryGetValue(key, out var repoValue))
                {
                    var hashedLocrKey = Crc32BHasher.Crc32(repoValue.ToUpperInvariant());
                    var locrValue = locrCache.TryGetValue(hashedLocrKey, out var value) ? value : repoValue;
                    file.LocalizedTextName = locrValue;
                }

                structure.Files.Add(file);
            }
        }

        structure.Directories = structure.Directories.OrderBy(x => x.Name).ToList();
        return structure;
    }

    public List<string> GetPermittedRoots()
    {
        return approvedRoots;
    }
}