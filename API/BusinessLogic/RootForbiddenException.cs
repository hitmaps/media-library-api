﻿namespace API.BusinessLogic;

public class RootForbiddenException : Exception
{
    public RootForbiddenException(string root) : base($"{root} is not an authorized root directory")
    {
    }
}