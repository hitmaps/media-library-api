﻿namespace API.BusinessLogic;

public interface IDirectoryStructureService
{
    Task<Directory> GetFolderStructureAsync(string? path = "");
    Task<List<File>> GetFilesAsync(string path);
    
    Task<DirectoryWithFiles> GetFolderStructureForRootAndDirectoryAsync(string root, string? directory = "", bool includeFiles = true);

    List<string> GetPermittedRoots();
}