﻿namespace API.BusinessLogic.IoiApi;

public interface IHitmapsIoiApiService
{
    Task<Dictionary<string, string>> GetRepoEntriesAsync();
    Task<Dictionary<uint, string>> GetLocalizedTextAsync();
}