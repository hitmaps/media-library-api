﻿namespace API.BusinessLogic.IoiApi;

public class ApiResponseWrapper<T>
{
    public List<T> Data { get; set; } = new();
}