﻿using System.Net.Http.Headers;
using System.Text.Json;

namespace API.BusinessLogic.IoiApi;

public class HitmapsIoiApiService : IHitmapsIoiApiService
{
    private const string ItemsEndpoint = "https://ioiapi.hitmaps.com/api/weapons";
    private const string DisguisesEndpoint = "https://ioiapi.hitmaps.com/api/disguises";
    private readonly HttpClient httpClient;

    public HitmapsIoiApiService()
    {
        httpClient = new HttpClient();
        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        httpClient.DefaultRequestHeaders.Add("User-Agent", "HITMAPS Media Library API");
    }

    public async Task<Dictionary<string, string>> GetRepoEntriesAsync()
    {
        var combined = await GetImagePathToNameLocrKeyAsync(ItemsEndpoint);
        var disguises = await GetImagePathToNameLocrKeyAsync(DisguisesEndpoint);
        foreach (var entry in disguises)
        {
            combined.TryAdd(entry.Key, entry.Value);
        }

        return combined;
    }

    private async Task<Dictionary<string, string>> GetImagePathToNameLocrKeyAsync(string uri)
    {
        var results = new Dictionary<string, string>();
        using var request = new HttpRequestMessage(HttpMethod.Get, uri);
        try
        {
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var data = (await JsonSerializer.DeserializeAsync<ApiResponseWrapper<ApiResponse>>(await response.Content.ReadAsStreamAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }))!.Data;

            foreach (var item in data.Where(x => x.Image != null))
            {
                results.TryAdd(item.Image!, item.Name);
            }
        }
        catch (HttpRequestException)
        {
            return new Dictionary<string, string>();
        }

        return results;
    }

    public async Task<Dictionary<uint, string>> GetLocalizedTextAsync()
    {
        var dictionary = new Dictionary<uint, string>();
        using var request = new HttpRequestMessage(HttpMethod.Get, "https://ioiapi.hitmaps.com/api/localized-text");
        try
        {
            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            var data = (await JsonSerializer.DeserializeAsync<ApiResponseWrapper<LocalizedTextApiResponse>>(
                await response.Content.ReadAsStreamAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                }))!.Data;

            foreach (var entry in data)
            {
                dictionary.TryAdd(entry.Hash, entry.Text);
            }
        }
        catch (HttpRequestException)
        {
            return new Dictionary<uint, string>();
        }

        return dictionary;
    }
}