﻿namespace API.BusinessLogic.IoiApi;

public class Entry
{
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
}