﻿namespace API.BusinessLogic.IoiApi;

public class LocalizedTextApiResponse
{
    public int Id { get; set; }
    public uint Hash { get; set; }
    public string LanguageCode { get; set; } = null!;
    public string Text { get; set; } = null!;
}