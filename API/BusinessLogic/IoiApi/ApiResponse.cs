﻿namespace API.BusinessLogic.IoiApi;

public class ApiResponse
{
    public int Id { get; set; }
    public Guid InternalId { get; set; }
    public string Name { get; set; } = null!;
    public string? Description { get; set; }
    public string? Image { get; set; }
}