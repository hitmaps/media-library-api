﻿namespace API.BusinessLogic;

public class File
{
    public string FileName { get; private set; }
    public string? LocalizedTextName { get; set; }
    public string FullPath { get; set; } = null!;

    public File(string fileName)
    {
        FileName = Path.GetFileName(fileName);
    }
}