﻿using Amazon.S3;
using Amazon.S3.Model;
using API.BusinessLogic.IoiApi;

namespace API.BusinessLogic;

public class BackblazeDirectoryStructureService : IDirectoryStructureService
{
    private readonly List<string> approvedRoots;
    private readonly string bucketName;
    private readonly IHitmapsIoiApiService hitmapsIoiApiService;
    private readonly IAmazonS3 s3Client;

    private Dictionary<string, string> repoCache = new();
    private Dictionary<uint, string> locrCache = new();
    private Dictionary<string, DirectoryWithFiles> b2Cache = new();

    public BackblazeDirectoryStructureService(IConfiguration configuration, 
        IHitmapsIoiApiService hitmapsIoiApiService, 
        IAmazonS3 s3Client)
    {
        approvedRoots = configuration.GetValue<string>("ApprovedMediaLibraryRoots")
            .Split(';')
            .Select(x => x.ToLowerInvariant())
            .ToList();
        bucketName = configuration.GetValue<string>("Backblaze:BucketName");
        this.hitmapsIoiApiService = hitmapsIoiApiService;
        this.s3Client = s3Client;
    }

    public Task<Directory> GetFolderStructureAsync(string? path = "")
    {
        throw new NotImplementedException();
    }

    public Task<List<File>> GetFilesAsync(string path)
    {
        throw new NotImplementedException();
    }

    public async Task<DirectoryWithFiles> GetFolderStructureForRootAndDirectoryAsync(string root, string? directory = "", bool includeFiles = true)
    {
        if (!approvedRoots.Contains(root.ToLowerInvariant()))
        {
            throw new RootForbiddenException(root);
        }

        if (!repoCache.Any())
        {
            repoCache = await hitmapsIoiApiService.GetRepoEntriesAsync();
        }

        if (!locrCache.Any())
        {
            locrCache = await hitmapsIoiApiService.GetLocalizedTextAsync();
        }

        if (!string.IsNullOrWhiteSpace(directory))
        {
            directory += "/";
        }
        else
        {
            directory = string.Empty;
        }
        var prefix = $"img/{root}/{directory}";
        if (b2Cache.TryGetValue(prefix, out var cachedDirectory))
        {
            return cachedDirectory;
        }

        var structure = new DirectoryWithFiles(directory);
        string continuationToken = null;

        do
        {
            var backblazeResponse = await s3Client.ListObjectsV2Async(new ListObjectsV2Request
            {
                Delimiter = "/",
                BucketName = bucketName,
                Prefix = prefix,
                ContinuationToken = continuationToken
            });
            continuationToken = backblazeResponse.NextContinuationToken;

            structure.Directories.AddRange(backblazeResponse.CommonPrefixes
                .Select(backblazeDirectory => new DirectoryWithFiles(backblazeDirectory.Replace(prefix, "")))
                .ToList());
            structure.Files.AddRange(backblazeResponse.S3Objects
                .Select(s3File =>
                {
                    var file = new File(s3File.Key.Replace(prefix, ""))
                    {
                        FullPath = s3File.Key
                    };
                    var key = s3File.Key.Replace($"img/{root}/", "images/")
                        .Replace(Path.DirectorySeparatorChar, '/');

                    // ReSharper disable once InvertIf
                    if (repoCache.TryGetValue(key, out var repoValue))
                    {
                        var hashedLocrKey = Crc32BHasher.Crc32(repoValue.ToUpperInvariant());
                        var locrValue = locrCache.TryGetValue(hashedLocrKey, out var value) ? value : repoValue;
                        file.LocalizedTextName = locrValue;
                    }

                    return file;
                }).ToList());
        } while (!string.IsNullOrWhiteSpace(continuationToken));
        
        b2Cache[prefix] = structure;
        
        return structure;
    }

    public List<string> GetPermittedRoots()
    {
        return approvedRoots;
    }
}