﻿namespace API.BusinessLogic;

public class Directory
{
    public string Name { get; private set; }

    public Directory(string path)
    {
        Name = path == string.Empty ? string.Empty : new DirectoryInfo(path).Name;
    }

    public List<Directory> Subdirectories { get; set; } = new();
}