﻿namespace API.BusinessLogic;

[Obsolete("Use Directory instead")]
public class DirectoryWithFiles
{
    public string Name { get; private set; }

    public DirectoryWithFiles(string path)
    {
        Name = path == string.Empty ? string.Empty : new DirectoryInfo(path).Name;
    }

    public List<DirectoryWithFiles> Directories { get; set; } = new();
    public List<File> Files { get; set; } = new();
}