using Amazon.Runtime;
using Amazon.Runtime.Endpoints;
using Amazon.S3;
using API.BusinessLogic;
using API.BusinessLogic.IoiApi;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddApiVersioning(options =>
{
    options.DefaultApiVersion = new ApiVersion(1, 0);
    options.ReportApiVersions = true;
    options.AssumeDefaultVersionWhenUnspecified = true;
    options.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
});
/*builder.Services.AddSingleton<IAmazonS3>(new AmazonS3Client(new BasicAWSCredentials(
        builder.Configuration.GetValue<string>("Backblaze:KeyId"), 
        builder.Configuration.GetValue<string>("Backblaze:ApplicationKey")),
    new AmazonS3Config
    {
        ServiceURL = "https://s3.us-west-002.backblazeb2.com"
    }));*/
builder.Services.AddSingleton<IDirectoryStructureService, FileSystemDirectoryStructureService>();
builder.Services.AddSingleton<IHitmapsIoiApiService, HitmapsIoiApiService>();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(o => o.AddPolicy("CorsPolicy", x =>
    x.AllowAnyMethod()
        .AllowAnyHeader()
        .WithOrigins("http://localhost:3000", "https://mediabrowser.hitmaps.com")));

var app = builder.Build();

app.UseCors("CorsPolicy");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Apply EF migrations if in production, as we use command line otherwise
if (app.Environment.IsProduction())
{
    app.UseExceptionHandler("/error");
}

app.UseForwardedHeaders(new ForwardedHeadersOptions
{
    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();